__author__ = 'Anton'
import sys
import StringIO
import os
import BrochureSpaces
from weasyprint import HTML

storage = BrochureSpaces.pg.Storage()
box_obj = BrochureSpaces.box.Box(storage=storage)

kwargs = {'edit_area': sys.argv[1], 'file_link': sys.argv[2], 'temp_text_file_template': sys.argv[3],
          'client_data_folder_id': sys.argv[4], 'pdf_id': sys.argv[5]}

pdf_string = StringIO.StringIO()
#print str(soup)
edit_area = open(kwargs['edit_area'], 'r')
print 'pdfs started'

html = HTML(string=edit_area.read())
#subprocess.check_output(["echo", "Hello World!"])
html.write_pdf(pdf_string)
#html.write_pdf('test_pdf.pdf')

print 'pdfs generated'

file_name = kwargs['file_link'].split('.')[0] + '.pdf'
if not kwargs['pdf_id'] or kwargs['pdf_id'] == 'None':
    pdf_id = box_obj.saveFile(folder_id=kwargs['client_data_folder_id'], content=pdf_string, file_name=file_name)
    print pdf_id, kwargs['file_link']
    storage.cursor.execute('UPDATE print_jobs SET pdf_id= %s WHERE file_name = %s', [pdf_id, kwargs['file_link']])
    storage.con.commit()
else:
    box_obj.updateFile(file_id=kwargs['pdf_id'], file_name=file_name, content=pdf_string.getvalue())
