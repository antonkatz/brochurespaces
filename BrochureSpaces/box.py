#This class has to be written to be initiated on every connection
#This way it will allow for an easy user initiated restart

#For now the control will look for a folder with the right name, but later on it will have to create it and rename it
from gevent import monkey, lock, Greenlet; monkey.patch_all()

from PIL import Image, ImageFile
from StringIO import StringIO
import settings
import requests
import cPickle
import psycopg2.extras
import logging
import json
from datetime import datetime, timedelta


#This class is pulled from adminmod. some of the functions have incompatible features like emit
class Box:
    def __init__(self, **kwargs):
        #Setting up
        self.glock = lock.Semaphore()
        self.requests = requests
        self.pg = kwargs['storage']

        self.queries = settings.box_queries
        self.client_id = settings.box_data['client_id']
        self.client_secret = settings.box_data['client_secret']
        self.retoucher_folder_id = settings.box_data['retoucher_folder_id']
        self.html_folder_id_last = None  # Last looked up html_folder for saving pages. A dict when filled.

        self.box_data = None
        self.auth_header_template = 'Bearer {0}'
        self.auth_header = {'Authorization': None}

        self.next_refresh = datetime.now()
        #First step is to get the token
        #Check memory (requests), then db
        if not self.box_data:
            try:
                self.pg.cursor.execute(self.queries['retrieve'])
            except:
                self.pg.cursorReload()
                self.pg.cursor.execute(self.queries['retrieve'])

            result = self.pg.cursor.fetchone()
            self.box_data = cPickle.loads(result['pickle'])
            #This is for starting up the process when there is no token saved yet, on first grant of refresh token
            #self.box_data = {'refresh_token': 'evwrTgItwPVqEpaiBWf51F12CRAj3iEF3YxmXm2TNNjGFhhbkFX1brBixM2nCXAl'}

        self.refreshToken()
        #folder_id = self.execRequest(self.findFolder, {'folder_name': '5420fallingbrook48', 'parent': settings.box_data['retoucher_folder_id']})['folder_id']

    def downloadFolder(self, folder_id, file_type=None, meta_only=False, object_type='file'):
        self.refreshToken()
        self.glock.acquire()
        #Downloads all files in a folder, and then yields {contents, file_id, name} one by one
        #file type (is a list) controls which type of file gets downloaded
        #Generator function
        #Mode meta_only. if true will not download.
        url = 'https://api.box.com/2.0/folders/{0}/items'.format(folder_id)
        files = requests.get(url, headers=self.auth_header)
        for file in files.json()['entries']:
            if file['type'] == object_type:
                if file_type:
                    ext = file['name'].split('.')[::-1][0]
                    if not ext in file_type:
                        continue

                if not meta_only and object_type != 'folder':  # !=folder is a precaution
                    url = 'https://api.box.com/2.0/files/{0}/content'.format(file['id'])
                    try:
                        content = requests.get(url, headers=self.auth_header).content
                    except:
                        logging.error('Box could not download the folder', exc_info=True)
                else:
                    content = None
                #If looking up folders, file_id is not the best description
                yield {'content': content, 'file_id': file['id'], 'name': file['name']}
        self.glock.release()

    def downloadFile(self, **kwargs):
        self.refreshToken()
        self.glock.acquire()

        url = 'https://api.box.com/2.0/files/{0}/content'.format(kwargs['file_id'])
        try:
            content = requests.get(url, headers=self.auth_header).content
            #self.glock.release()
            return content
        except:
            logging.error('Box could not download the file %s', kwargs['file_id'], exc_info=True)
        finally:
            self.glock.release()

    def reduceSize(self, image_file_obj, ext=None):
        self.glock.acquire()
        #Taken from the original box app. Modified to work only with JPEGs (ext set to None).
        image_store = StringIO(image_file_obj)
        image_save = StringIO()
        image = Image.open(image_store)

        try:
            #image.save(image_save, self.config.pil_format[ext], quality=self.settings.blog_image_quality, optimize=True)
            image.save(image_save, 'JPEG', quality=settings.blog_image_quality, optimize=True)
        except IOError:
            ImageFile.MAXBLOCK = image.size[0] * image.size[1]
            image.save(image_save, 'JPEG', quality=settings.blog_image_quality, optimize=True)
            #image.save(image_save, self.config.pil_format[ext], quality=self.settings.blog_image_quality, optimize=True)

        jpeg = image_save.getvalue()
        image_store.close()
        image_save.close()

        self.glock.release()
        return jpeg

    def findFolder(self, folder_name, parent=None):
        self.refreshToken()
        self.glock.acquire()
        #Returns the request obj if no parent specified. Otherwise returns dict {status_code, folder_id}
        #Searches for a folder that has parent in path_collection. Returns first found.
        url = 'https://api.box.com/2.0/search'
        params = {'query': folder_name}
        request = requests.get(url, params=params, headers=self.auth_header)
        if parent:
            data = request.json()['entries']
            for entry in data:
                path = entry['path_collection']['entries']
                for folder in path:
                    if int(folder['id']) == parent:
                        self.glock.release()
                        return {'status_code': request.status_code, 'folder_id': entry['id']}
            self.glock.release()
            return False
        else:
            self.glock.release()
            return {'status_code': request.status_code, 'request_object': request}

    def execRequest(self, func, kwargs={}):
        self.glock.acquire()
        #For executing requests. On 401 will refresh token and try again.
        #func must return request obj, or status_code (possible in a dict)
        try:
            r = func(**kwargs)
            if isinstance(r, dict):
                status_code = r['status_code']
            elif isinstance(r, int):
                status_code = r
            elif not r:
                return False
            else:
                status_code = r.status_code

            if status_code == 401:
                self.refreshToken()
                r = func(**kwargs)
            return r

        except BaseException as e:
            self.refreshToken()
            try:
                r = func(**kwargs)
                return r
            except BaseException as e:
                print('Could not execute request', e)
                raise
        self.glock.release()

    def refreshToken(self, response=None, forced=False):
        self.glock.acquire()
        #Checks response status. If invalid_token refreshes. If no response given refreshes.
        #Then saves to DB
        flag = True
        if response:
            if not response.status_code == 401:
                flag = False

        # Checking if the token is close to expiration, and returning if it's not
        now = datetime.now()
        if now < self.next_refresh:
            logging.info('Box token has not expired yet')
            self.glock.release()
            return
        self.next_refresh = now + timedelta(minutes=50)

        if flag or forced:
            params = {'grant_type': 'refresh_token', 'refresh_token': self.box_data['refresh_token'], 'client_id': self.client_id, 'client_secret': self.client_secret}
            refresh = requests.post('https://www.box.com/api/oauth2/token', data=params)
            if refresh.status_code == 200:
                self.box_data = refresh.json()
                logging.debug(self.box_data)
                _pickle = psycopg2.extensions.adapt(cPickle.dumps(self.box_data))
                query = self.queries['save'].format(_pickle)
                try:
                    self.pg.cursor.execute(query)
                    self.pg.con.commit()
                except BaseException as e:
                    try:
                        self.pg.cursorReload()
                        self.pg.cursor.execute(query)
                        self.pg.con.commit()
                    except BaseException as e2:
                        print('Could not save refreshed box token', e, e2)
                    finally:
                        self.auth_header['Authorization'] = self.auth_header_template.format(self.box_data['access_token'])
                        self.glock.release()
                #Updating authorization header to use in requests
                self.auth_header['Authorization'] = self.auth_header_template.format(self.box_data['access_token'])
                self.glock.release()
            else:
                logging.error('Could not refresh box token' + str(refresh.content))
        self.glock.release()

    def listMusic(self, folder_id):
        self.glock.acquire()
        #Yields music links that have both types of files present. (name, [links], [types])
        types = ['ogg', 'mp3']
        _temp = {}

        try:
            url = 'https://api.box.com/2.0/folders/{0}/items'.format(folder_id)
            self.refreshToken()
            files = requests.get(url, headers=self.auth_header)
            for file in files.json()['entries']:
                if file['type'] == 'file':
                    url = 'https://api.box.com/2.0/files/{0}'.format(file['id'])
                    try:
                        link = requests.put(url, headers=self.auth_header, data='{"shared_link": {"access": "open"}}').json()
                        name, type = link['name'].split('.')[0], link['name'].split('.')[1]
                        if not name in _temp:
                            _temp[name] = {}
                            _temp[name]['types'] = []
                            _temp[name]['links'] = []

                        _temp[name]['types'].append(type)
                        _temp[name]['links'].append(link['shared_link']['download_url'])
                    except:
                        self.session['endpoints']['form'].emit('error', 'Could not get music files')
        except BaseException as e:
            self.session['endpoints']['form'].emit('error', 'Listing music failed: '+str(e))

        # Checking if all types are present
        for file_name, value in _temp.iteritems():
            _types = list(value['types'])
            for i, type in enumerate(value['types'][::-1]):
                if type in types:
                    _types.remove(type)
            if not _types:
                yield (file_name, value['links'], value['types'])

        self.glock.release()

    def savePage(self, **kwargs):
        #Takes folder_name (the name of the template), the original page (png) name
        #There will be a temporary class variable {folder_name, id} so that the process doesn't have to relook for the folder id
        #Redundant with the save file function?
        self.refreshToken()
        self.glock.acquire()
        flag_in_memory = False
        store_folder_id = None

        #If the folder id is given no need to look it up
        if 'folder_id' not in kwargs:
            if self.html_folder_id_last:
                if kwargs['folder_name'] in self.html_folder_id_last:
                    store_folder_id = self.html_folder_id_last[kwargs['folder_name']]
                    flag_in_memory = True

            if not flag_in_memory:
                url = 'https://api.box.com/2.0/folders/{0}/items'.format(settings.box_data['html_folder_id'])
                objects = requests.get(url, headers=self.auth_header)
                for obj in objects.json()['entries']:
                    if obj['type'] == 'folder':
                        if obj['name'] == kwargs['folder_name']:
                            store_folder_id = obj['id']
        else:
            store_folder_id = kwargs['folder_id']

        #if not store_folder_id:
        #    store_folder_id = self.createTemplate(folder_name=kwargs['folder_name'])

        url = 'https://upload.box.com/api/2.0/files/content'
        post_data = {"parent_id": store_folder_id}
        files = {kwargs['page_name']+".html": StringIO(kwargs['content'])}
        upload_request = requests.post(url=url, data=post_data, files=files, headers=self.auth_header)

        id = upload_request.json()['entries'][0]['id']
        link = self.shareFile(skip_refresh=True, file_id=id)

        self.glock.release()
        return {'id': id, 'link': link}

    def createFolder(self, **kwargs):
        #SO FAR JUST CREATES THE APPROPRIATE FOLDER, AND MAYBE SHOULD BE LEFT THIS WAY AND RENAMED CREATE FOLDER
        #Takes folder_name; returns id on success
        #Creates a folder for the template and saves all the needed data in db though a function written in pg class.
        #Returns the newly created folder id
        self.refreshToken()
        self.glock.acquire()
        try:
            url = 'https://api.box.com/2.0/folders'
            post_data = {"name": kwargs['folder_name'], "parent": {"id": settings.box_data['html_folder_id']}}
            post_data = json.dumps(post_data)
            new_folder = requests.post(url=url, data=post_data, headers=self.auth_header)
            if new_folder.status_code > 299:
                raise Exception('Box request failed.' + new_folder.content)
            id = new_folder.json()['id']
            logging.debug('createFolder. Creating folder %s.', id)
            self.html_folder_id_last = {kwargs['folder_name']: id}  # Don't exactly remember what this is for, might be useless
            self.glock.release()
            return id
        except BaseException:
            logging.error('Could not save page because folder could not be created', exc_info=True)
            raise
        finally:
            self.glock.release()

    def shareFile(self, **kwargs):
        if not 'skip_refresh' in kwargs:
            self.refreshToken()
        else:
            logging.info('shareFile has skipped token refresh')
        self.glock.acquire()

        url = 'https://api.box.com/2.0/files/{0}'.format(kwargs['file_id'])
        share_link = requests.put(url=url, data='{"shared_link": {"access": "open", "permissions": {"can_download": true}}}', headers=self.auth_header)
        try:
            share_link = share_link.json()
            share_link = share_link['shared_link']['download_url']
            self.glock.release()
            return share_link
        except:
            logging.error('Could not get share link for file id %s. Response %s', kwargs['file_id'], share_link.content, exc_info=True)
        finally:
            self.glock.release()

    def shareFolderFiles(self, **kwargs):
        #Shares all files in a folder
        #Gives a list at the end. If needed as generator just use code inside
        #No locks in this function because it consists of functions that already took care of that problem
        links_only = []
        try:
            if not 'file_type' in kwargs:
                kwargs['file_type'] = None
            for _file in self.downloadFolder(folder_id=kwargs['folder_id'], file_type=kwargs['file_type'], meta_only=True):
                link = self.shareFile(file_id=_file['file_id'], skip_refresh=True)
                links_only.append(link)
        except:
            logging.error('Could not gather all the share links from folder id %s', kwargs['folder_id'], exc_info=True)
        finally:
            return links_only

    def saveFile(self, **kwargs):
        #Saves files
        #If search parameter present, searches for the file with the same name, takes the first result and updates it (dangerous)
        self.refreshToken()
        self.glock.acquire()

        if 'search' in kwargs:
            url = 'https://api.box.com/2.0/search'
            params = {'query': kwargs['file_name']}
            request = requests.get(url, params=params, headers=self.auth_header)
            json = request.json()
            logging.info('saveFile found %s files with the same name', json['total_count'])
            if json['total_count'] != 0:
                new = False
                conflict = json['entries'][0]['id']
            else:
                new = True
        else:
            new = True

        if new:
            url = 'https://upload.box.com/api/2.0/files/content'
            post_data = {"parent_id": kwargs['folder_id']}
            #WTF is get value for? Stream objects?
            files = {kwargs['file_name']: kwargs['content'].getvalue()}
            upload_request = requests.post(url=url, data=post_data, files=files, headers=self.auth_header)
            try:
                if upload_request.status_code > 299:
                    raise BaseException(upload_request.content)
                id = upload_request.json()['entries'][0]['id']
                logging.info('Saved file %s', kwargs['file_name'])
                self.glock.release()
                return id
            except:
                logging.warning('box.saveFile Could not save file named %s.', kwargs['file_name'], exc_info=True)
            finally:
                self.glock.release()
        else:
            try:
                self.updateFile(file_id=conflict, file_name=kwargs['file_name'], content=kwargs['content'].getvalue())
                return conflict
            except:
                logging.error('Could not update', exc_info=True)
            finally:
                self.glock.release()

    def updateFile(self, **kwargs):
        #Saves the cutout file to the appropriate folder
        self.refreshToken()
        self.glock.acquire()

        url = 'https://upload.box.com/api/2.0/files/{0}/content'.format(kwargs['file_id'])
        #post_data = {"parent_id": kwargs['folder_id']}
        files = {kwargs['file_name']: kwargs['content']}
        upload_request = requests.post(url=url, files=files, headers=self.auth_header)
        #data=post_data,

        try:
            if upload_request.status_code > 299:
                raise BaseException(upload_request.content)
            id = upload_request.json()['entries'][0]['id']
            logging.info('Updated file %s', kwargs['file_name'])
            self.glock.release()
            return id
        except:
            logging.error('box.saveFile Could not update file id %s', kwargs['file_id'], exc_info=True)
        finally:
            self.glock.release()