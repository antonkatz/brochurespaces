import gevent, gevent.monkey
gevent.monkey.patch_all()

from PIL import Image, ImageDraw
import logging
import copy
import numpy
from scipy import ndimage
import gc

class rawImage:
    def __init__(self, **kwargs):
        #logging.root.setLevel(logging.DEBUG)

        self.p_data = []    # Positions of all non opaque pixels

        self.image = Image.open(kwargs['image_file'])

        gevent.spawn(self.findTransparent, **kwargs).join()
        gevent.spawn(self.findViewPorts, **kwargs).join()
        gevent.spawn(self.findBoundaries, **kwargs).join()
        gevent.spawn(self.readViewPorts, **kwargs).join()

        #Will need to be deleted in production version
        self.image.save('data_frontend/image_v2.png', "PNG")

        logging.debug('rawImage ran successfully')

    def findTransparent(self, **kwargs):
        self.image = self.image.convert("RGBA")
        reduce_by = 10
        if self.image.size * self.image.size[1] < 100000:
            reduce_by = 1
        self.reduced_w, self.reduced_h = self.image.size[0] / reduce_by, self.image.size[1] / reduce_by
        image = self.image.resize((self.reduced_w, self.reduced_h), Image.ANTIALIAS)
        data = image.getdata()
        #data = self.image.getdata()

        c_data = []     # NOT NEEDED. Converted data
        #extremes = {'top': None, 'bottom': None, 'left': None, 'right': None}
        x, y = 0, 0
        width = self.reduced_w
        #width = self.image.size[0]
        for pixel in data:
            if x == width:    # Skipping from row to row of pixels
                x = 0
                y += 1

            if pixel[3] != 255:
                c_data.append((255, 0, 0, 255))
                self.p_data.append((x, y))
            else:
                c_data.append(pixel)
            x += 1

            # NOT NEEDED
            #self.image.putdata(c_data)
            #self.image.save('image.png', "PNG")

    def findViewPorts(self, **kwargs):
        logging.debug('findViewPorts start')
        self.view_ports = []

        max_x = self.reduced_w + 1
        max_y = self.reduced_h + 1
        #max_x = self.image.size[0]
        #max_y = self.image.size[1]

        logging.info('Matrix size %s x %s', max_x, max_y)
        image = numpy.zeros((max_x, max_y))

        for x, y in self.p_data:
            image[x, y] = 1
        labelled, number_of_ports = ndimage.label(image)

        logging.info('Number of ports: %s', number_of_ports)

        self.view_ports = [ [[], []] for i in range(number_of_ports)]
        for (x,y), v in numpy.ndenumerate(labelled):
            if v != 0:
                self.view_ports[int(v)-1][0].append(x)
                self.view_ports[int(v)-1][1].append(y)

    def _findViewPorts(self, **kwargs):
        # Takes data and divides it into chunks that are continuous
        logging.debug('findViewPorts start')
        self.view_ports = []    # Each entry is list of (x, y)

        # Take first pixel, find all adjacent, transfer and delete
        p_data = copy.copy(self.p_data)
        len_start = len(p_data)
        while p_data:
            #print 'pdata', p_data
            xp = range(p_data[0][0] - 1, p_data[0][0] + 2)      # + 2 because of the quirks of range
            yp = range(p_data[0][1] - 1, p_data[0][1] + 2)     # Possible x, y ranges of adjacent pixels

            _temp = []
            _temp.append((p_data[0][0], p_data[0][1]))
            del p_data[0]

            adjacent = filter(lambda p: p[0] in xp and p[1] in yp, p_data)
            o_adjacent = copy.copy(adjacent)
            deleted = []
            #print 'adj', adjacent
            #_adjacent = set()
            _adjacent = []

            for p in adjacent:
                deleted.append(p)
                p_data.remove(p)

            while adjacent:
                logging.debug('Current: %s, Start: %s', len(p_data), len_start)
                logging.debug('Percent done %s', int((1.0-float(len(p_data))/float(len_start))*100))
                pixel = adjacent[0]
                # Say we could expand the range to 10 pixels instead of 3
                xp = range(pixel[0] - 5, pixel[0] + 6)
                yp = range(pixel[1] - 5, pixel[1] + 6)     # Possible x, y ranges of adjacent pixels
                _temp.append((pixel[0], pixel[1]))
                #deleted.append((pixel[0], pixel[1]))
                try:
                    #p_data.remove(adjacent[0])
                    del adjacent[0]
                except BaseException as e:
                    logging.warning('findViewPorts. %s', e)
                    #return

                filtered = [p for p in p_data if p[0] in xp and p[1] in yp and p[0] != pixel[0] and p[1] != pixel[1]]
                #filtered = [p for p in p_data if (p[0] == pixel[0]-1 or p[0] == pixel[0]+1) and (p[1] == pixel[0]-1 or p[1] == pixel[1]+1)]
                #for p in filter(lambda p: p[0] in xp and p[1] in yp and p[0] != pixel[0] and p[1] != pixel[1], p_data):
                #for p in filter(lambda p: (p[0] == pixel[0]-1 or p[0] == pixel[0]+1) and (p[1] == pixel[0]-1 or p[1] == pixel[1]+1), p_data):
                for p in filtered:
                    if not p in deleted or not p in o_adjacent:
                        deleted.append(p)
                        #_adjacent.add(p)
                        _adjacent.append(p)
                        p_data.remove(p)

                if not adjacent:    # Making sure that adjacent don't repeat or includes deleted pixels
                    #adjacent = list(_adjacent)
                    adjacent = _adjacent
                    #print adjacent
                    o_adjacent = copy.copy(adjacent)
                    #for p in deleted:
                    #    try:
                    #        adjacent.remove(p)
                    #    except BaseException:
                            #This might be causing much grief in processing speeds
                    #        pass
                    #_adjacent = set()
                    _adjacent = []

            logging.debug('findViewPorts ' + str(_temp))
            self.view_ports.append(_temp)

    def findBoundaries(self, **kwargs):
        # Finds max, min values of each viewport. For now draws a line across.
        self.view_ports_bounds = []
        width, height = self.image.size
        for port in self.view_ports:
            #logging.info('findBoundries. New port analysis')
            #xmax, xmin, ymax, ymin = 0, width, 0, height

            #xmax = max(port[0])
            #xmin = min(port[0])
            #ymax = max(port[1])
            #ymin = min(port[1])
            xmax = max(port[0]) * 10
            xmin = min(port[0]) * 10
            ymax = max(port[1]) * 10
            ymin = min(port[1]) * 10

            if (xmax - xmin) * (ymax - ymin) == 0:
                continue

            self.view_ports_bounds.append((xmin, ymin, xmax, ymax))
            logging.debug('findBoundaries. Extremes. %s, %s, %s, %s (x, y, x, y)', xmin, ymin, xmax, ymax)

            #draw = ImageDraw.Draw(self.image)
            #draw.line((xmin, ymin, xmax, ymax), fill=(255, 0, 0, 255))

    def readViewPorts(self, **kwargs):
        # Cuts the viewport boxes (including opaque pixels), gets (--) inside, saves it
        self.view_ports_order = []
        self.loaded_image = self.image.load()
        for box in self.view_ports_bounds:
            x, y, xm, ym = box[0]+10, box[1]+10, box[2]-10, box[3]-10
            w = xm - x
            h = ym - y
            data = self.image.crop((x, y, xm, ym))
            flag = False
            for pixel in data.getdata():
                if pixel[3] == 255:
                    order = filter(lambda ord: ord['r'] == pixel[0] and ord['g'] == pixel[1] and ord['b'] == pixel[2], kwargs['order'])
                    if order:
                        flag = True
                        #print order
                        self.view_ports_order.append(order[0]['name'])
                        #break
            if not flag:
                logging.debug('Empty port. Assigned %s', kwargs['order'][0]['name'])
                self.view_ports_order.append(kwargs['order'][0]['name'])
            else:
                for cx in range(w):
                    for cy in range(h):
                        #logging.debug('Removing pixel %s,%s', cx+x, cy+y)
                        self.loaded_image[cx+x, cy+y] = (255, 255, 255, 0)