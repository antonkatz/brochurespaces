import logging
import copy

def fVP_cython(int p_data):
    view_ports = []  # This wil be returned

    while p_data:
        print 'pdata', p_data
        xp = range(p_data[0][0] - 1, p_data[0][0] + 2)      # + 2 because of the quirks of range
        yp = range(p_data[0][1] - 1, p_data[0][1] + 2)     # Possible x, y ranges of adjacent pixels

        _temp = []
        _temp.append((p_data[0][0], p_data[0][1]))
        del p_data[0]

        adjacent = filter(lambda p: p[0] in xp and p[1] in yp, p_data)
        o_adjacent = copy.copy(adjacent)
        deleted = []
        print 'adj', adjacent
        _adjacent = set()

        for p in adjacent:
            deleted.append(p)
            p_data.remove(p)

        while adjacent:
            #logging.debug(len(p_data)*100/len_start)
            pixel = adjacent[0]
            xp = range(pixel[0] - 1, pixel[0] + 2)
            yp = range(pixel[1] - 1, pixel[1] + 2)     # Possible x, y ranges of adjacent pixels
            _temp.append((pixel[0], pixel[1]))
            #deleted.append((pixel[0], pixel[1]))
            try:
                #p_data.remove(adjacent[0])
                del adjacent[0]
            except BaseException as e:
                logging.warning('findViewPorts. %s', e)
                #return

            for p in filter(lambda p: p[0] in xp and p[1] in yp and p[0] != pixel[0] and p[1] != pixel[1], p_data):
            #for p in filter(lambda p: (p[0] == pixel[0]-1 or p[0] == pixel[0]+1) and (p[1] == pixel[0]-1 or p[1] == pixel[1]+1), p_data):
                if not p in deleted or not p in o_adjacent:
                    deleted.append(p)
                    _adjacent.add(p)
                    p_data.remove(p)

            if not adjacent:    # Making sure that adjacent don't repeat or includes deleted pixels
                adjacent = list(_adjacent)
                print adjacent
                o_adjacent = copy.copy(adjacent)
                #for p in deleted:
                #    try:
                #        adjacent.remove(p)
                #    except BaseException:
                        #This might be causing much grief in processing speeds
                #        pass
                _adjacent = set()

        logging.debug('findViewPorts ' + str(_temp))
        view_ports.append(_temp)

    return view_ports