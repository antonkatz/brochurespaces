__author__ = 'anarchist'

import gevent, gevent.monkey
gevent.monkey.patch_all()

import logging
from .. import settings, settings_local
from jinja2 import Environment, FileSystemLoader
import math
import hashlib
import bs4
import pprint
import copy


class htmlProcessing:
    def __init__(self, **kwargs):
        #Takes box, image_size, bounds
        logging.info('htmlProcessing started')
        self.j_env = Environment(loader=FileSystemLoader('html_templates'))
        self.box = kwargs['box']

        #A little fuckup and my laziness
        class kwargs_container:
            def __init__(self):
                self.image_size, self.bounds = None, None
        self.image_size = []
        self.kwargs = kwargs_container()
        for size in kwargs['image_size']:
            self.image_size.append([_size/settings.ratio_pixel_inch for _size in size])
        self.kwargs.image_size, self.kwargs.bounds = self.reduceSize(image_size=kwargs['image_size'], bounds=kwargs['bounds'])

    def adminTemplate(self, **kwargs):
        #Creates the template for the admins. Creates a class var for the edit area.
        #Takes image_list, title, cutout_url (most likely those are lists)
        main_css_template = self.j_env.get_template('main.css')
        self.main_css_rendered = main_css_template.render(cutout_size=self.kwargs.image_size, view_ports=self.kwargs.bounds,
                                             image_list=kwargs['image_list'])

        print_css_template = self.j_env.get_template('print.css')
        self.print_css_rendered = print_css_template.render(page_size=self.image_size, cutout_size=self.kwargs.image_size)

        inline_template = self.j_env.get_template('admin_inline.html')
        self.inline_rendered = inline_template.render(cutout_src=kwargs['cutout_url'], print_css=self.print_css_rendered, title=kwargs['title'],
                                                      view_ports=self.kwargs.bounds, image_list=kwargs['image_list'], styles_gen=self.main_css_rendered,
                                                      backend_server_path=settings_local.backend_server_path)
        self.text_layers = ''

    def clientTemplate(self, **kwargs):
        #Creates the template for the admins. Creates a class var for the edit area.
        #Takes image_list, title, cutout_url (most likely those are lists)
        ordered_image_list = []
        copy_image_list = copy.copy(kwargs['image_list'])
        #print kwargs['order']
        for port_fill in kwargs['order']:
            for option in port_fill:
                image = filter(lambda im: im['name'] == option, copy_image_list)
                if image:
                    ordered_image_list.append(image[0])
                    copy_image_list.remove(image[0])
                    break
        ordered_image_list += copy_image_list

        main_css_template = self.j_env.get_template('main.css')
        self.main_css_rendered = main_css_template.render(cutout_size=self.kwargs.image_size, view_ports=self.kwargs.bounds,
                                             image_list=ordered_image_list)

        print_css_template = self.j_env.get_template('print.css')
        self.print_css_rendered = print_css_template.render(page_size=self.image_size, cutout_size=self.kwargs.image_size)

        inline_template = self.j_env.get_template('client_inline.html')
        self.inline_rendered = inline_template.render(cutout_src=kwargs['cutout_url'], print_css=self.print_css_rendered, title=kwargs['title'],
                                                      view_ports=self.kwargs.bounds, image_list=kwargs['image_list'], styles_gen=self.main_css_rendered,
                                                      text_layers=kwargs['text_layers'], backend_server_path=settings_local.backend_server_path)
        self.text_layers = kwargs['text_layers']
        #pprint.pprint(ordered_image_list)


    def reduceSize(self, **kwargs):
        # This function shrinks the image sizes (only in numbers, not the actual file)
        # This will need to be done by javascript because the screensize is unknown until loading time
        max_width, max_height = 0, 0
        #Getting max sizes
        for size in kwargs['image_size']:
            if size[0] > max_width:
                max_width = size[0]
            if size[1] > max_height:
                max_height = size[1]

        #Converting into percentage the cutout sizes
        return_image_size = []
        for size in kwargs['image_size']:
            w, h = float(size[0])/max_width, float(size[1])/max_height
            return_image_size.append([w*100, h*100])

        #Converting into percentage bounds
        return_bounds = []
        for i, port_bounds in enumerate(kwargs['bounds']):
            return_bounds.append([])
            for bounds in port_bounds:
                _bounds = []
                _bounds.append( math.ceil(float(bounds[0])*10000/max_width)/100 )
                _bounds.append( math.ceil(float(bounds[1])*10000/max_height)/100 )
                _bounds.append( math.ceil(float(bounds[2])*1000/max_width)/10 )
                _bounds.append( math.ceil(float(bounds[3])*1000/max_height)/10 )
                return_bounds[i].append(_bounds)

        return (return_image_size, return_bounds)

    def saveTepmlate_local(self, **kwargs):
        #Takes template_name, db_id, and type
        name = str(kwargs['template_name'])+str(kwargs['db_id'])+str(kwargs['type'])
        if kwargs['type'] == 'client':
            name = name + kwargs['order_id']
        md5 = hashlib.md5(name).hexdigest()
        file_name = md5 + '.html'
        path = settings.local_store_path + file_name
        save = open(path, 'w')

        soup = bs4.BeautifulSoup(self.inline_rendered)
        soup.find(id='file_link')['value'] = file_name
        soup.find(id='access_type')['value'] = kwargs['type']

        save.write(str(soup))
        save.close()
        return file_name

    def storeBox(self, **kwargs):
        #Basically serves as an alias for the box function. (For now.)
        return self.box.savePage(folder_id=kwargs['folder_id'], page_name=kwargs['page_name'], content=self.text_layers)

    def returnTemplate(self, **kwargs):
        return self.inline_rendered
# There will be another function, boxStore_client

    #def storeLocal(self):
    #    self.saveTepmlate_local(file='index2.html', data=self.main_rendered)
    #    self.saveTepmlate_local(file='styles_gen.css', data=self.main_css_rendered)
    #    self.saveTepmlate_local(file='print.css', data=self.print_css_rendered)

