import gevent.monkey, gevent.lock; gevent.monkey.patch_all()
import psycopg2, psycopg2.extras
import settings
import logging


class Storage:
    def __init__(self):
        self.reload_lock = gevent.lock.Semaphore()
        self.connectRetry()

    def connectRetry(self):
        try:
            self.connect()
        except:
            connected = False
            for i in range(1, 6):
                logging.warning('Retrying connection. %s', i)
                try:
                    self.connect()
                    connected = True
                    break
                except BaseException as e:
                    gevent.sleep(5)
            if not connected:
                logging.error('Could not establish connection to db. %s', str(e), exc_info=True)

    def connect(self):
        self.con = psycopg2.connect(host=settings.pg_host, password=settings.pg_pass, user=settings.pg_user, database=settings.pg_db)
        self.cursor = self.con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    def cursorReload(self):
        self.reload_lock.acquire()
        try:
            self.cursor.close()
            self.con.close()
            self.con = psycopg2.connect(host=settings.pg_host, password=settings.pg_pass, user=settings.pg_user, database=settings.pg_db)
            self.cursor = self.con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        except:
            #self.con = psycopg2.connect(host=settings.pg_host, password=settings.pg_pass, user=settings.pg_user, database=settings.pg_db)
            self.connectRetry()

        self.reload_lock.release()