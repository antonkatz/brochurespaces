__author__ = 'anarchist'
import gevent, gevent.monkey, gevent.lock; gevent.monkey.patch_all()

import rawProcessing
import box
import pg
import StringIO
from datetime import datetime
import settings
import settings_local
import logging
import cPickle
import gc
import bs4
from weasyprint import HTML, CSS
import base64
import os
import re
import subprocess
from memory_profiler import profile

import smtplib
from email.mime.multipart import MIMEMultipart, MIMEBase
from email.mime.text import MIMEText


class BrochureSpaces:
    def __init__(self, **kwargs):
        self.storage = pg.Storage()
        self.box_obj = box.Box(storage=self.storage)
        self.pdf_lock = gevent.lock.Semaphore()
        self.temp_png_path = 'tmp/{0}.png'
        self.generate_text_greenlet = 0

    def sendEmail(self, **kwargs):
        logging.debug('Sending email')
        to = settings_local.email_to
        subject = 'PDF GENERATION::{0}'.format(kwargs['order_slug'])
        if kwargs['pdf'] == False:
            text = 'Pdf generation started. Order {0}'.format(kwargs['order_slug'])
        elif not kwargs['pdf']:
            text = 'Pdf generation ended. No link obtained. Possibly did not generate pdf correctly OR could not obtain link'
        else:
            text = 'Pdf generation ended. Link {0}'.format(kwargs['pdf'])

        recipient = to
        msg = MIMEMultipart()
        msg['From'] = settings.gmail_user
        msg['To'] = recipient
        msg['Subject'] = subject

        msg.attach(MIMEText(text))

        #part = MIMEBase('application', 'octet-stream')
        #part.set_payload(open(attach, 'rb').read())
        #Encoders.encode_base64(part)
        #part.add_header('Content-Disposition',
        #        'attachment; filename="%s"' % os.path.basename(attach))
        #msg.attach(part)

        mailServer = smtplib.SMTP("smtp.gmail.com", 587)
        mailServer.ehlo()
        mailServer.starttls()
        mailServer.ehlo()
        mailServer.login(settings.gmail_user, settings.gmail_pass)
        mailServer.sendmail(settings.gmail_user, recipient, msg.as_string())
        # Should be mailServer.quit(), but that crashes...
        mailServer.close()

    def writePdf(self, **kwargs):
        self.pdf_lock.acquire()
        #Writes pdf, replaces photos with high quality, send notification email
        #Also saves if it's shipping or not, though it should not
        logging.info('Writing PDF and saving to box')

        #Switching up links
        query_get_order = "SELECT order_id, pdf_id FROM print_jobs WHERE file_name=%s"
        self.storage.cursor.execute(query_get_order, [kwargs['file_link']])
        print_jobs_data = self.storage.cursor.fetchone()
        order_id = print_jobs_data['order_id']

        query_get_slug = "SELECT slug FROM orders WHERE order_id = %s"
        self.storage.cursor.execute(query_get_slug, [order_id])
        slug = self.storage.cursor.fetchone()['slug']
        self.sendEmail(order_slug=slug, pdf=False)

        query_get_image_ids = "SELECT box_id, server_link FROM image_links WHERE order_id=%s"
        self.storage.cursor.execute(query_get_image_ids, [order_id])
        image_list = self.storage.cursor.fetchall()
        for row in image_list:
            pattern = "background-image: url('http://localhost:8081/blogs/{0}');".format(row['server_link'])
            share_link = self.box_obj.shareFile(file_id=row['box_id'])
            pattern = "background-image: url('{0}');".format(share_link)
            kwargs['edit_area'] = re.sub(pattern, share_link, kwargs['edit_area'])

        temp_filename = 'tmp/ea_{0}.data'.format(kwargs['file_link'])
        temp_file = open(temp_filename, 'w')
        temp_file.write(kwargs['edit_area'])
        temp_file.close()

        sub_args = ' {0} {1} {2} {3} {4} {5}'.format('generatePdf.py', temp_filename, kwargs['file_link'], self.temp_png_path,
                                                     settings.box_data['client_data_folder_id'], print_jobs_data['pdf_id'])
        sub_proc = subprocess.Popen(settings_local.path_to_python + sub_args, shell=True, executable="/bin/bash")
        while sub_proc.poll() == None:
            logging.debug('Waiting on PDF generation and upload')
            gevent.sleep(20)
        logging.info('PDF subprocess done')
        self.box_obj.refreshToken(forced=True)
        #gevent.sleep(300) #doesn't help

        query_get_pdf = "SELECT pdf_id FROM print_jobs WHERE file_name = %s"
        self.storage.cursor.execute(query_get_pdf, [kwargs['file_link']])
        pdf_id = self.storage.cursor.fetchone()['pdf_id']
        pdf_link = self.box_obj.shareFile(file_id=pdf_id)
        self.sendEmail(order_slug=slug, pdf=pdf_link)

        try:
            os.remove(self.temp_text_file_template.format(kwargs['file_link']))
            os.remove(temp_filename)
        except:
            pass

        collected = gc.collect()
        logging.debug('Collected %s garbage', collected)
        self.pdf_lock.release()

    #diplayEdit seem to be remains of another time.
    def displayEdit(self, **kwargs):
        print kwargs
        folder_id = self.box_obj.findFolder(kwargs['folder_name'], settings.box_data['html_folder_id'])
        print folder_id
        if isinstance(folder_id, dict):
            folder_content = self.box_obj.downloadFolder(folder_id=folder_id['folder_id'], file_type=['html', 'htm'])
            for _file in folder_content:
                return _file
        else:
            logging.error("Could not find folder")
            raise BaseException('Box function findFolder failed to return results from query')

    #Here will go a function that get's all the new folders and creates all necessary stuff for them
    def parseNew(self, **kwargs):
        #Only parses new. What to do if a folder is deleted?
        query = 'SELECT COUNT(id) FROM bs_templates WHERE png_box_folder_id = %s'
        query_store = 'INSERT INTO bs_templates (png_box_folder_id, name) VALUES (%(folder_id)s, %(name)s) RETURNING id'
        query_store_update = 'UPDATE bs_templates SET link = %s, text_layers_id = %s, titles = %s, pickle_pages_list = %s, direct_link = %s WHERE id = %s'
        #Since I'm lazy I'll just pickle pages list, and be done with it

        self.storage.cursorReload()
        for obj in self.box_obj.downloadFolder(folder_id=settings.box_data['png_folder_id'], object_type='folder', meta_only=True):
            self.storage.cursor.execute(query, [obj['file_id']])
            count = self.storage.cursor.fetchone()['count']
            if count == 0:
                logging.info('parseNew. Found new png folder. Id %s. Name %s', obj['file_id'], obj['name'])
                storage_folder_id = self.box_obj.createFolder(folder_name=obj['name'])

                self.storage.cursor.execute(query_store, {'folder_id': obj['file_id'], 'name': obj['name']})
                self.storage.con.commit()
                template_db_id = self.storage.cursor.fetchone()['id']

                #additional_data = self.processTemplate(folder_id=obj['file_id'], folder_name=obj['name'], db_id=template_db_id)
                additional_data = self.processTemplate(folder_id=obj['file_id'], folder_name=obj['name'], db_id=template_db_id, storage_folder_id=storage_folder_id)
                #print settings_local.server_brochure_access_path + link
                pickle_pages_list = cPickle.dumps(additional_data['pages_list'])
                self.storage.cursor.execute(query_store_update, [additional_data['server_link'], additional_data['text_layers_id'], additional_data['titles'],
                                                                 pickle_pages_list, settings_local.server_brochure_access_path+additional_data['server_link'],
                                                                 template_db_id])
                self.storage.con.commit()

    def processTemplate(self, **kwargs):
        #This will process a new template. Get's contents of a new folder and turns everything into an editable format
        #Does other things like saving to DB
        #Takes folder_id, folder_name, storage_folder_id as arguments
        pages_list = {'bounds': [], 'size': [], 'cutout_url': [], 'title': [], 'order': []}
        folder_contents = self.box_obj.downloadFolder(kwargs['folder_id'])
        for _file in folder_contents:
            #_file is info from custom function and not the object given by box
            split_filename = _file['name'].split('.')
            filename = split_filename[0]
            extension = split_filename[::-1][0]
            if extension == 'png':
                image_file = StringIO.StringIO(_file['content'])
                img_data = rawProcessing.rawImage(image_file=image_file, order=settings.image_port_order)
                #This will need to be removed

                stringio_image = StringIO.StringIO()
                img_data.image.save(stringio_image, format="PNG")
                cutout_id = self.box_obj.saveFile(file_name=_file['name'], folder_id=kwargs['storage_folder_id'], content=stringio_image)
                share_link_cutout = self.box_obj.shareFile(file_id=cutout_id)

                pages_list['bounds'].append(img_data.view_ports_bounds)
                pages_list['size'].append(img_data.image.size)
                pages_list['cutout_url'].append(share_link_cutout)
                pages_list['title'].append(filename)
                pages_list['order'] += img_data.view_ports_order

                logging.info('Finished processing %s', _file["name"])
                logging.debug('Collected %s garbage', gc.collect())

        #For now test image links are pickled.  This is where the test images folder would go. Maybe even replace this with a set list stored somewhere.
        image_list = cPickle.load(open(settings.test_images_pickle_file))
        html_obj = rawProcessing.htmlProcessing(box=self.box_obj, bounds=pages_list['bounds'], image_size=pages_list['size'])
        html_obj.adminTemplate(image_list=image_list, cutout_url=pages_list['cutout_url'], title=pages_list['title'])

        text_layers_id = html_obj.storeBox(folder_id=kwargs['storage_folder_id'], page_name='text_layers')['id']
        link = html_obj.saveTepmlate_local(template_name=kwargs['folder_name'], db_id=kwargs['db_id'], type='admin')
        return {'server_link': link, 'titles': pages_list['title'], 'text_layers_id': text_layers_id, 'pages_list': pages_list}
        #return html_obj.returnTemplate()

    def copyTemplate(self, **kwargs):
        #Takes template_id, and image list in form {name, server_link}
        #Makes a copy. Fills out the automatic text. (Before this part can be done, save function must be written)
        if not 'reload_false' in kwargs: self.storage.cursorReload()

        #Get edit area id (then download the file itself) and the titles for the pages
        query = "SELECT text_layers_id, titles, pickle_pages_list FROM bs_templates WHERE id = %s"
        self.storage.cursor.execute(query, [kwargs['template_db_id']])
        fetched = self.storage.cursor.fetchone()
        text_layers_id = fetched['text_layers_id']

        text_layers = self.box_obj.downloadFile(file_id=text_layers_id)
        titles = fetched['titles']
        pages_list = cPickle.loads(fetched['pickle_pages_list'])

        #kwargs['image_list']
        html_obj = rawProcessing.htmlProcessing(box=self.box_obj, bounds=pages_list['bounds'], image_size=pages_list['size'])
        html_obj.clientTemplate(image_list=kwargs['image_list'], cutout_url=pages_list['cutout_url'], title=pages_list['title'],
                                text_layers=text_layers, order=pages_list['order'])
        link = html_obj.saveTepmlate_local(template_name='', db_id=kwargs['template_db_id'], type='client', order_id=kwargs['order_id'])
        print settings_local.server_brochure_access_path + link
        return link

    def newOrder(self, **kwargs):
        #This copies all the templates in the order
        self.storage.cursorReload()

        logging.debug("newOrder. Copying templates from order - %s", kwargs['order_id'])
        query = "SELECT template_id FROM orders WHERE order_id=%s"
        self.storage.cursor.execute(query, [kwargs['order_id']])
        template_ids = self.storage.cursor.fetchone()['template_id']

        #Retrieving photo links.
        query = "SELECT name, server_link FROM image_links WHERE order_id = %s"
        self.storage.cursor.execute(query, [kwargs['order_id']])
        image_list = self.storage.cursor.fetchall()
        for image in image_list:
            image['server_link'] = settings_local.server_blog_access_path + image['server_link']

        #Checking if in printjobs, and if not letting a query run later
        query_print_jobs = "SELECT COUNT(job_id) FROM print_jobs WHERE order_id=%s"
        self.storage.cursor.execute(query_print_jobs, [kwargs['order_id']])
        print_job_count = self.storage.cursor.fetchone()['count']
        query_print_jobs = "INSERT INTO print_jobs (order_id, template_id, link, file_name, ship, pdf, qty, estimate, printer_status, shipping_info)" \
                           "VALUES (%s, %s, %s, %s, %s, %s, 0, now(), FALSE, 'NONE')"

        links = []
        for id in template_ids:
            link = self.copyTemplate(template_db_id=id, image_list=image_list, order_id=kwargs['order_id'], reload_false=True)
            links.append(link)
            if print_job_count == 0:
                self.storage.cursor.execute(query_print_jobs, [kwargs['order_id'], id, settings_local.server_brochure_access_path+link, link, False, 'NULL'])

        query = "UPDATE orders SET template_links = %s WHERE order_id = %s"
        self.storage.cursor.execute(query, [links, kwargs['order_id']])
        self.storage.con.commit()

    def saveTemplate(self, **kwargs):
        #Rewrites the html file. If admin type then also rewrites the text in the box file.
        #takes file_link, edit_area, type
        logging.info('Saving %s. Type - %s', kwargs['file_link'], kwargs['type'])
        brochure = open(settings.local_store_path+kwargs['file_link'])
        soup = bs4.BeautifulSoup(brochure)
        brochure.close()

        edit_area_tag = soup.new_tag('div')
        edit_area_tag['id'] = 'edit_area'
        edit_area_tag.string = kwargs['edit_area']
        edit_area = soup.find(id='edit_area').replace_with(edit_area_tag)


        brochure = open(settings.local_store_path+kwargs['file_link'], 'w')
        brochure.write(soup.encode(formatter=None))
        brochure.close()

        if kwargs['type'] == 'admin':
            query = 'SELECT text_layers_id FROM bs_templates WHERE link=%s'
            self.storage.cursor.execute(query, [kwargs['file_link']])
            fetched = self.storage.cursor.fetchone()

            soup = bs4.BeautifulSoup(kwargs['edit_area'])
            text_layers = soup.find_all(class_='cutout_page')
            text_layers_string = ''
            for layer in text_layers:
                text_layers_string = text_layers_string + str(layer)

            self.box_obj.updateFile(file_id=fetched['text_layers_id'], file_name='text_layers.html', content=text_layers_string)

        collected = gc.collect()
        logging.debug('saveTemplate collected %s garbage', collected)

    #For managing many incoming requests
    #def generateText_(self, **kwargs):
    #    if self.generate_text_greenlet == 0:
    #        self.generate_text_greenlet = gevent.spawn(self.generateText, **kwargs)
    #        self.generate_text_greenlet.join()
    #    else:
    #        if not self.generate_text_greenlet.ready():
    #            self.generate_text_greenlet.kill()
    #            self.generate_text_greenlet = gevent.spawn(self.generateText, **kwargs)
    #            self.generate_text_greenlet.join()
    #        else:
    #            self.generate_text_greenlet = gevent.spawn(self.generateText, **kwargs)
    #            self.generate_text_greenlet.join()
    #    return self.generate_text_greenlet.value

    def generateText(self, **kwargs):
        logging.debug('Generating text image')
        soup = bs4.BeautifulSoup(kwargs['box'])
        text_box = soup.find(class_='text_div_contents')

        css = '.text_div_contents {width: 100%; position:absolute;top:0;left:0;}' \
                '@page{width: '+kwargs['width']+'px; height: '+kwargs['height']+'px;margin:0;padding:0;}'\
                'p {margin: 5px;}' \
                #'border: 1px solid red;}' \
                #'span {border: 1px solid yellow;}' \
                #'div {border: 1px solid blue;}'
        stylesheet = CSS(string=css)

        #stringio = StringIO.StringIO()
        if not kwargs['file_link']:
            kwargs['file_link'] = 'tmp'
        file_name = self.temp_png_path.format(kwargs['file_link'])
        text_box = text_box.encode('utf-8')
        text_box = re.sub('>[^0-9A-Za-z]{2}</p>', '></p>', text_box, re.M)
        HTML(string=text_box).write_png(stylesheets=[stylesheet], target=file_name)

        image = open(file_name)
        base64_string = base64.b64encode(image.read())
        image.close()
        #os.remove(file_name)

        base64_string = 'data:image/png;base64,'+base64_string

        collected = gc.collect()
        logging.debug('Finished. Collected %s garbage', collected)
        return base64_string