__author__ = 'anarchist'

import gevent, gevent.monkey
gevent.monkey.patch_all()
from BrochureSpaces import BrochureSpaces
import logging

from bottle import route, run, request, static_file

logging.basicConfig(format='%(name)s\t%(levelname)s:%(message)s\t%(asctime)s', level=logging.DEBUG)

bs_obj = BrochureSpaces()

@route('/text', method="POST")
def generate_text():
    box = request.forms.get('box')
    width = request.forms.get('width')
    height = request.forms.get('height')
    file_link = request.forms.get('file_link')
    try:
        g = gevent.spawn(bs_obj.generateText, box=box, width=width, height=height, file_link=file_link)
        g.join()
        return g.value
    except:
        logging.error('Could not generate text', exc_info=True)

@route('/save', method="POST")
def save_page():
    try:
        edit_area = request.forms.get('edit_area')
        file_link = request.forms.get('file_link')
        type = request.forms.get('access_type')
        bs_obj.saveTemplate(edit_area=edit_area, file_link=file_link, type=type)
    except:
        logging.error('Could not save', exc_info=True)

edit_return_buffer = None

@route('/submit', method="POST")
def submit_page():
    try:
        edit_area = request.forms.get('edit_area')
        file_link = request.forms.get('file_link')
        gevent.spawn_later(0, bs_obj.writePdf, file_link=file_link, edit_area=edit_area)
        #bs_obj.writePdf(file_link=file_link, edit_area=edit_area)
    except:
        logging.error('Could not save pdf', exc_info=True)
    logging.debug('writePDF successfully deferred with gevent')

@route('/new', method="GET")
def process_page():
    try:
        bs_obj.parseNew()
    except:
        logging.error('Could not process new pngs', exc_info=True)

@route('/new_order/<order_id>', method="GET")
def newOrder(order_id):
    try:
        bs_obj.newOrder(order_id=order_id)
    except:
        logging.error('Could not make a new template', exc_info=True)

@route('/edit/<folder_name>')
def edit(folder_name):
    try:
        page = bs_obj.displayEdit(folder_name=folder_name)
        return page['content']
    except:
        logging.error('Error displaying edit page', exc_info=True)
        return 'There was an error processing your request. Please try again later.'



#def _process_page():
#    bs_obj.processTemplate(folder_id=1230127123)
#    BrochureSpaces.run(box=box_obj, storage=storage)

#def _edit(folder_name, file_name):
#    page = bs_obj.displayEdit(folder_name=folder_name)
#    return page['content']

run(host='0.0.0.0', port=8082)