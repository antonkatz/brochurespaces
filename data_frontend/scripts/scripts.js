//Set in ready()
var edit_area_w = 0;
var edit_area_h = 0;

//var list_images = ['test_images/masterbedroom1.jpg', 'test_images/masterbedroom2.jpg', 'test_images/masterbedroom3.jpg', 'test_images/lowerlevel5.jpg', 'test_images/lowerlevel6.jpg', 'test_images/lowerlevel4.jpg', 'test_images/livingroom1.jpg']

var preload_screen_id = '#preload';
var lightbox_id = '#lightbox';
var lightbox_outer_id = '#lightbox_outer';
var close_lightbox = '#close_lightbox';
var text_editor_id = '#text_editor';
var text_editor_id_wrapper = '#text_editor_wrapper';
var text_editor_id_ = 'text_editor';
var edit_area_id = '#edit_area';
var text_layer_class = 'text_layer';

var move_icon_class = 'move_icon';
var resize_icon_class = 'resize_icon';
var delete_icon_class = 'delete_icon';
var edit_icon_class = 'edit_icon';
var add_text_class = 'edit_text';
var text_div_contents = '.text_div_contents';
var text_div_image = '.text_div_image';
var zoom_in = "#zoom_in";
var zoom_out = "#zoom_out";
var edit_class = 'edit';
var edit_hover = 'hover';
var menu_height = 150;
var base_line_height = 100;

var change_photo_selector = '.image_div .edit_icon';
var image_div_frame = '.image_div_frame';
var image_div = '.image_div';
var image_container_id = '#image_list';
var image_list_class = 'image';
var image_list_slider = '#image_list_slider';

var cutout_page = '.cutout_page';
//var text_container_class = 'text_container';

var submit_input_id = '#submit_input';
var submit_form_id = '#submit_form';
var submit_button_id = '#submit_button';

var file_link_id = "#file_link"
var access_type_id = "#access_type"

//Global vars
var lightbox_status = 0;
var text_editor_status = -1;
var change_photo_status = -1;
var current_text_box = null;
var current_text_box_image = null;
var current_text_to_gen = '';
var current_photo = null;
var current_lightbox_side = null;
var current_image_change = null;
var current_image_change_url = null;
var current_zoom = 2;
var zoom_levels = [90, 70, 50];
var zoom_levels_text = [100, 77, 56];

var editor_instance = null;
var current_text_boxes_coords = null;

//Bounds of current element being dragged
var drag_left = 0;
var drag_top = 0;
var resize_start_x = 0;
var resize_start_y = 0;

//Styling vars
var ckeditor_height = 0 //This is set in ready
var image_edit_shown = 0; //1 if the editing icon is shown for an image
var text_layer_z_up = 500;
var text_layer_z_down = 300;

var can_send_text = true;
var can_save = true;

var secondary_menu_status = 1;
var secondary_menu = '#secondary_menu';
var just_resized = false;

function lightbox(mouse_event){
    //This function shows 100% x 100% box on top of everything.
    //First argument is the lightbox element
    //Second argument determines which sub-box is shown (text or image selector)

    console.log('lightbox status', lightbox_status, mouse_event);
    if (lightbox_status == 0) {
        var cursor_x = mouse_event.pageX;
        if (cursor_x <= $(window).width()/2){
            current_lightbox_side = 'right';
            $(lightbox_id).addClass(current_lightbox_side);
            $(lightbox_outer_id).addClass(current_lightbox_side);
        }else{
            current_lightbox_side = 'left';
            $(lightbox_id).addClass(current_lightbox_side);
            $(lightbox_outer_id).addClass(current_lightbox_side);
        }
        //console.log( 'lightbox', $(window).width()/2, cursor_x);

        $(lightbox_id).show();
        $(lightbox_outer_id).show();
        lightbox_status = 1;
        //$(document.body).on("click", lightbox_outer_id, function(e){console.log('close lightbox outer');closeLightbox();});
        $(document.body).on("click", close_lightbox, function(e){console.log('close lightbox');closeLightbox();});
        //console.log($(document));
    }else{
        //Sometimes on close a null is passed instead of mouse event
        $(lightbox_id).removeClass(current_lightbox_side);
        $(lightbox_outer_id).removeClass(current_lightbox_side);

        $(lightbox_id).hide();
        $(lightbox_outer_id).hide();
        lightbox_status = 0;
        //$(document.body).off("click", lightbox_outer_id);
        $(document.body).off("click", close_lightbox);

    }
}

function closeLightbox(e){
    console.log('closeLightbox text photo', text_editor_status, change_photo_status);
    if (text_editor_status == 1) {
        //var text = tinyMCE.activeEditor.getContent();
        //var text = editor_instance.getData();
        //current_text_box.html(text);
        //generateText();
        textEditor();
    }
    if (change_photo_status == 1) {
        changePhoto();
    }
    lightbox(null);
    saveFunction();
}

function textEditor(){
    //This function is showing the text editor
    if (text_editor_status == -1){
        $(text_editor_id).show();
        var editor = CKEDITOR.replace(text_editor_id_, {
            height: ckeditor_height
        });
        editor_instance = editor;

            console.log('editor init', ckeditor_height);
        if (current_text_box != null){
            editor.setData(current_text_box.html());
        }else{
            editor.setData('Insert Text Here');
        }


        editor.on('change', function(){
            var data = editor.getData();
            if (data == ''){
                data = 'Insert Text Here';
            }
            generateText(data);
        });
        text_editor_status = 0; //to be preinitialized
        $(text_editor_id_wrapper).hide();
    }else if (text_editor_status == 0){
        var editor = editor_instance;
        //console.log(current_text_box.html());
        editor.setData(current_text_box.html());

        $(text_editor_id_wrapper).show();
        //editor.show();
        text_editor_status = 1;
    }else{
        console.log('editor hide');
        var data = editor_instance.getData();
        if (data == ''){
                data = 'Insert Text Here';
        }
        generateText(data);
        $(text_editor_id_wrapper).hide();
        text_editor_status = 0;
    }
}

function generateText(text, auto){
    console.log('just_resized', just_resized);
    if (just_resized == true){
        just_resized = false;
    }else{
        if (text == undefined || text == ''){text = 'Insert Text Here';}
    }
    if (text != undefined && auto != true){current_text_box.html(text);}
    console.log('generateText', text, can_send_text, auto)
    if (can_send_text == true){
        //console.log('generateText', can_send_text);
        changeLineSpacing();
        if (!auto == true){
            can_send_text = false;
            setTimeout(function(){can_send_text=true;generateText(text, true)}, 1000);
        }

        var w = current_text_box.parent().width();
        var h = current_text_box.parent().height();
        var file_link = $(file_link_id).attr('value');
        $.post(backend_server_path+'text', {'box': current_text_box.parent().html(), 'width': w, 'height': h, 'file_link': file_link}, function(base64){
    //        console.log('current image box', current_text_box_image);
            $(current_text_box_image).css('background-image', 'url('+base64+')');
        });
    }
//        current_text_box_image.css('background-image', "url('data:"+base64+"');");
        //current_text_box_image.attr('src', base64);
}

function changeLineSpacing(){
    console.log('changeLineSpacing');
    var parent = current_text_box.parent();
    var parent_h = parent.height();
    var current_height = base_line_height;
    var last_height = null;
    var step = 100;
    var step_decrease = 0.5;
    var update = true;

    while (Math.abs(parent_h - current_text_box.height()) > 5){
        update = true;
        current_height += step;
        current_text_box.css('line-height', current_height+'%');
        if ((parent_h - current_text_box.height()) < 0){
            step = step*step_decrease;
            console.log('step', step, 'last', last_height);
            current_height = last_height + step;
            current_text_box.css('line-height', current_height+'%');
            console.log(current_height);
            update = false;
            if (step < 3){break;}
        }
        console.log(update);
        if (update){last_height = current_height;}
    }
}

function addText(edit_area, div_class, max_w, max_h, min_w, min_h, t, l){
    //Function for adding new text
    //Creates div, and adds icons and functionality
    //console.log('addText', edit_area, div_class, max_w, max_h, min_w, min_h, t, l);
    var text_div = $('<div></div>');
    var text_div_contents = $('<div class="text_div_contents"></div>');
    var text_div_image = $('<div class="text_div_image">');
    var move_icon = $('<div></div>');
    var resize_icon = $('<div></div>');
    var delete_icon = $('<div></div>');
    var edit_icon = $('<div></div>');

    text_div.addClass(div_class);
    text_div.addClass(edit_class);
    text_div.css('max-width', max_w);
    //text_div.css('max-height', max_h);
//    text_div.css('min-width', min_w);
    text_div.css('min-height', min_h);
    text_div.css('width', min_w);
    text_div.css('height', min_h);
    text_div.css('top', t);
    text_div.css('left', l);
    //text_div.on('mouseover', function(e){text_div.addClass(edit_hover); edit_icon.show();$('.'+text_layer_class).css('z-index', text_layer_z_up);});
    text_div.on('mouseenter', function(e){text_div.addClass(edit_hover); resize_icon.show();delete_icon.show();move_icon.show();});
    text_div.on('mouseleave', function(e){text_div.removeClass(edit_hover); resize_icon.hide(); delete_icon.hide();move_icon.hide();
                                            text_div.parent().css('z-index', text_layer_z_down);});

    //$('.'+text_layer_class).css('z-index', text_layer_z_down);

    move_icon.addClass(move_icon_class);
    move_icon.addClass(edit_class);
    resize_icon.addClass(resize_icon_class);
    resize_icon.addClass(edit_class);
    delete_icon.addClass(delete_icon_class);
    delete_icon.addClass(edit_class);
    edit_icon.addClass(edit_icon_class);
    edit_icon.addClass(edit_class);

    text_div_contents.css('line-height', base_line_height+'%');

    edit_area.append(text_div);
    $(text_div).append(text_div_contents);
    $(text_div).append(text_div_image);
    $(text_div).append(move_icon);
    $(text_div).append(resize_icon);
    $(text_div).append(delete_icon);
    $(text_div).append(edit_icon);

    text_div.draggable({ handle: move_icon, containment: '#edit_area', stop: dragStop});
    resize_icon.draggable({start: resizeStart, drag: resize, containment: '#edit_area', stop: resizeStop});
    delete_icon.click(function(){text_div.remove(); if (lightbox_status==1){lightbox();textEditor();}; saveFunction();});

    text_div_image.click(function(e){
        current_text_box = text_div_contents;
        current_text_box_image = text_div_image;

        if(lightbox_status==1){
            text_editor_status = 0;
        //    lightbox
        }else{
            lightbox(e);
        }
        textEditor();
    });
    text_div_image.css('background-size', zoom_levels_text[current_zoom]+'% '+zoom_levels_text[current_zoom]+'%');

    current_text_box = text_div_contents;
    current_text_box_image = text_div_image;

    resize_icon.hide(); delete_icon.hide();move_icon.hide();
}

function restoreText(){
    var textboxes = $(edit_area_id).find('.'+add_text_class);
    textboxes.each(function(){
        //console.log(this);
        var text_div = $(this);
        var text_div_contents = text_div.children('.text_div_contents');
        var text_div_image = text_div.children('.text_div_image');
        var move_icon = text_div.children('.'+move_icon_class);
        var resize_icon = text_div.children('.'+resize_icon_class);
        var delete_icon = text_div.children('.'+delete_icon_class);
        var edit_icon = text_div.children('.'+edit_icon_class);

        text_div.removeClass(edit_hover);
        edit_icon.hide();
        //edit_icon.show()
        text_div.on('mouseenter', function(e){text_div.addClass(edit_hover); resize_icon.show();delete_icon.show();move_icon.show();});
        text_div.on('mouseleave', function(e){text_div.removeClass(edit_hover); resize_icon.hide(); delete_icon.hide();move_icon.hide();
                                                text_div.parent().css('z-index', text_layer_z_down);});
        text_div.draggable({ handle: move_icon, containment: '#edit_area', stop: dragStop});
        resize_icon.draggable({start: resizeStart, drag: resize, containment: '#edit_area', stop: resizeStop});
        delete_icon.click(function(){text_div.remove(); if (lightbox_status==1){lightbox();textEditor();}; saveFunction();});
        text_div_image.click(function(e){
//            console.log('restoreText', text_div_contents.html());
            current_text_box = text_div_contents;
            current_text_box_image = text_div_image;

            if(lightbox_status==1){
                text_editor_status = 0;
            //    lightbox
            }else{
                lightbox(e);
            }
            textEditor();
        });

        resize_icon.hide(); delete_icon.hide();move_icon.hide();
    });
    $(text_div_image).css('background-size', zoom_levels_text[current_zoom]+'% '+zoom_levels_text[current_zoom]+'%');
}

function resizeStart(e){
    resize_start_x = e.pageX;
    resize_start_y = e.pageY;

    var parent = $(e.target).parent();
    current_text_box = parent.children(text_div_contents);
    current_text_box_image = parent.children(text_div_image);
}

function resize(e){
    add_x = e.pageX - resize_start_x;
    add_y = e.pageY - resize_start_y;
    resize_start_x = e.pageX
    resize_start_y = e.pageY

    var parent = $(e.target.parentNode);
    var parent_text_layer = parent.parent()
    var w = parent.width();
    var h = parent.height();
    parent.css('width', ((add_x + w) * 100 / parent_text_layer.width())+'%');
    parent.css('height', ((add_y + h) * 100 / parent_text_layer.height())+'%');
}

function resizeStop(e){
    //changeLineSpacing();
    just_resized = true;
    generateText();
    $('.resize_icon').attr('style', '');
    setTimeout(saveFunction, 500);
}

function dragStart(e){
    var w = $(e.target).width();
    var h = $(e.target).height();
    drag_left = edit_area_w - w;
    drag_top = edit_area_h - h;
    console.log(drag_left, drag_top);
}

function drag(e){
    var target = $(e.target);
    var position = target.position();

    if (position.top < 1){
        target.css('top', '1px');
        return false;
    }
    if (position.top > drag_top){
        target.css('top', drag_top+'px');
        return false;
    }
    if (position.left < 1){
        target.css('left', 1);
        return false;
    }
    if (position.left > drag_left){
        target.css('left', drag_left+px);
        return false;
    }
}

function dragStop(e){
    var box = $(e.target);
    var parent = box.parent();
    var top = box.position().top * 100 / parent.height();
    var left = box.position().left * 100 / parent.width();
    box.css('top', top+'%');
    box.css('left', left+'%');
    saveFunction();
}

function changePhoto(e){
    console.log('change photo', change_photo_status);
    if (change_photo_status <= 0){
        $(text_editor_id).hide();
        $(image_container_id).show();
        //current_photo = $(e.target.parentElement);
        console.log('cp', current_image_change);
        if (change_photo_status == -1){ //The first use
            listReplacePhotos();
        }
        change_photo_status = 1;
        lightbox(e);
    }else{
        change_photo_status = 0;
        $(image_container_id).hide();
        closeLightbox();
    }
}

function listReplacePhotos(){
    //Lists photos and sets the function to actually replace
    console.log('list_replace');
    var count = list_images.length;
    var i = 0;
    var changed = 1;
    //$(image_container_id).html('');
    while (i < count) {
        //var hold_i = i;
        //var image = $('<div></div>');
        var wrapper = $('<div style="display: inline-block; position: relative; width: 31.3%; margin-right: 0.6%; border:1px solid black;">' +
                            '<div style="display: inline-block; position: absolute; width: 100%; bottom: 0; left: 0; text-align: center; background-color: #FFFFF2">'+
                            list_images[i][1]+'</div></div>')
        var image = $('<img style="float:left; width: 100%; margin: 0;">');
        image.addClass(image_list_class);
        //image.css('background-image', 'url("'+list_images[i]+'")');
        image.attr('src', list_images[i][0]);
        image.click(function(e){
            //current_image_change.css('background-image', $(e.target).css('background-image'));
            current_image_change.css('background-image', $(e.target).attr('src'));
            //console.log($(e.target).css('background-image'));
            changePhoto();
            changed = 1; //I forsee problems
            //lightbox(e);
        });
        image.on('mouseover', function(e){
            console.log('mouseover changed', changed);
            if (changed == 1){
                current_image_change_url = current_image_change.css('background-image');
                changed = 0;
            }
            current_image_change.css('background-image', 'url("'+$(e.target).attr('src')+'")');
            //current_image_change.attr('src', $(e.target).attr('src'));
        });
//        image.on('mouseout', function(){
//            console.log('replace photo changed', changed, current_image_change_url);
//            if (changed != 1){
//                current_image_change.css('background-image', current_image_change_url);
//                //current_image_change.css('background-image', current_image_change_url);
//            }
//        });

        $(wrapper).append(image);
        $(image_container_id).append(wrapper);
        i++;
    }
    $(lightbox_id).on('mouseleave', function(){
        console.log('replace photo changed', changed, current_image_change_url);
        if (changed != 1){
            current_image_change.css('background-image', current_image_change_url);
            //current_image_change.css('background-image', current_image_change_url);
        }
    });
}

//This function selects which page to add text to based on the pages position relative to browser window
function selectTextLayer(){
    var layers = $('.'+text_layer_class);
    var l_length = layers.length
    var i = 0;
    while (i < l_length){
        var layer_top = $(layers[i]).offset().top;
        var layer_half_height = $(layers[i]).height() / 2;
        var layer_scroll = layer_top - $(window).scrollTop();
        var layer_scroll_bottom = layer_top - $(window).scrollTop() + $(layers[i]).height();
        var middle_position = layer_half_height + layer_scroll;
        console.log('bottom', layer_scroll_bottom);
        console.log(middle_position, layer_scroll, ($(window).height()/2));
        //if (middle_position <= ($(window).height()/2) && middle_position > 0){
        if (layer_scroll_bottom > ($(window).height()/2)){
            return {'edit_area': $(layers[i]), 'position': Math.abs(layer_scroll - $(window).height()/2)};
            console.log('layer text', layers[i]);
        }
        i++;
    }
    return {'edit_area': $(layers[0]), 'position': Math.abs(layer_scroll - $(window).height()/2)};
}

function fitPagetoCutout(){
    console.log('Fitted page to cutout');
    var cutout_pages = $(cutout_page); var cutout_pages_count = cutout_pages.length; var i = 0;
    //fitting cutout div to the size of the cutout image
    while (i < cutout_pages_count){
        var cutout = $(cutout_pages[i]).find('img');
        $(cutout_pages[i]).height(cutout.height());
        i++;
    }
}

function saveFunction(){
    if (can_save == true){
        //can_save = false;
        //setTimeout(function(){can_save=true;saveFunction()}, 10000);
        var html = $(edit_area_id).html();
        var file_link = $(file_link_id).attr('value');
        var access_type = $(access_type_id).attr('value');
        console.log('Saving', file_link);

        $.post(backend_server_path+'save', {'edit_area': html, 'file_link': file_link, 'access_type': access_type});
    }
}

function secondaryMenu(){
    var menu = $(secondary_menu);
    if (secondary_menu_status == 1){
        menu.hide();
        secondary_menu_status = 0;
    }else{
        menu.show();
        secondary_menu_status = 1;
    }
}

$(document).ready(function(){
    ckeditor_height = $(window).height() - 310;
    restoreText();
    textEditor();

    $(text_editor_id).hide();

    $('#add_text').on('click', function(e){

        var text_layer_data = selectTextLayer();
        var edit_area = text_layer_data['edit_area'];
        console.log('text_layer', edit_area, text_layer_data['position'])
        var position = text_layer_data['position'];
        position = position * 100 / edit_area.height();

        //var edit_area = $(edit_area_id);
        //var position = $(window).scrollTop() + menu_height + 10; //Don't forget to convert to percent

        //addText(edit_area, add_text_class, edit_area_w+'px', edit_area_h+'px', '50px', '10px', position+'px', '10px');
        addText(edit_area, add_text_class, '50%', edit_area_h+'px', '48%', '10%', position+'%', '1%');
        console.log('add text event--');
        if (lightbox_status == 1){
            closeLightbox(e); //In case the lightbox is already open
        }
        console.log('add text event', e);
        lightbox(e);
        textEditor();
    });

    //$(change_photo_selector).click(changePhoto);

    $(submit_button_id).click(function(){
        var html = $(edit_area_id).html();
        var file_link = $(file_link_id).attr('value');
        var access_type = $(access_type_id).attr('value');
        var delivery = $('[name=pickup_option]:checked').val();
        console.log('submit delivery', delivery);
        alert('Approved for print!');
        saveFunction();

        $.post(backend_server_path+'submit', {'edit_area': html, 'file_link': file_link});
    });
    $(zoom_out).click(function(){
        var zoom_level_count = zoom_levels.length;
        if ((current_zoom + 1 ) < zoom_level_count){
            current_zoom++;
            $(edit_area_id).css('width', zoom_levels[current_zoom]+'%');
            $(text_div_image).css('background-size', zoom_levels_text[current_zoom]+'% '+zoom_levels_text[current_zoom]+'%');
            fitPagetoCutout();
        }
    });
    $(zoom_in).click(function(){
        if ((current_zoom - 1 ) > -1){
            current_zoom--;
            $(edit_area_id).css('width', zoom_levels[current_zoom]+'%');
            $(text_div_image).css('background-size', zoom_levels_text[current_zoom]+'% '+zoom_levels_text[current_zoom]+'%');
            fitPagetoCutout();
        }
    });
    $(edit_area_id).css('width', zoom_levels[current_zoom]+'%');
    $(text_div_image).css('background-size', zoom_levels_text[current_zoom]+'% '+zoom_levels_text[current_zoom]+'%');
    fitPagetoCutout();

    //Styling vars


    //Work vars
    edit_area_h = 1000; //These need to be dynamic
    edit_area_w = 1000;

    //Style functions
    listReplacePhotos(); change_photo_status = 0; //preloading images into image_list selector
    $(image_list_slider).slider({
                range: "max",
                min: 1,
                max: 3,
                value: 1,
                slide: function( event, ui ) {
                    var per_row = 4 - ui.value;
                    var w = (100 - (2*per_row)) / per_row;
                    $(image_container_id+' .'+image_list_class).parent().css('width', w+'%');
                }});

    //Gets fitted too many times. ok for now
    $('img').load(function(){
        fitPagetoCutout();
    });

    //$('.'+edit_icon_class).click(changePhoto);
    $(image_div_frame).click(changePhoto);
    $(image_div_frame).on('mouseover', function(e){
        //This is dangerous way of looking for classes. A shift in the index, and it doesn't work anymore.
        if (e.target.classList.length > 1) {
            var class_img = $(e.target).data('img');
        }else{
            class_img = e.target.classList[0]
        }

        var edit_icon = null;
        var image = null;
        if (class_img == edit_icon_class){
            edit_icon = $(e.target);
            class_img = $(e.target).parent().data('img');
        }else{
            edit_icon = $(e.target).find('.'+edit_icon_class);
        }
        image = $(image_div+'.'+class_img);
        current_image_change = image; //set the variable for changePhoto function
        image.addClass(edit_hover);

        edit_icon.show();

        current_text_boxes_coords = [];
        var text_layer = $(e.target).parent().children('.text_layer');
        var text_boxes = text_layer.children('.edit_text');
        text_boxes.each(function(){
            var box = $(this);
            var offset = box.offset();
            var x1 = offset.left;
            var y1 = offset.top;
            var x2 = x1 + box.width();
            var y2 = y1 + box.height();
            current_text_boxes_coords.push({'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2, 'box': box});
        });
    });
    $(image_div_frame).on('mouseleave', function(e){
        //var class_img = e.target.classList[1];
        //var image = $(image_div+'.'+class_img);
        //var edit_icon = $(e.target).find('.'+edit_icon_class);

        $('.'+edit_icon_class).hide();
        $(image_div).removeClass(edit_hover);
        //console.log('leave', edit_icon, image, e.target);

    });
    $(image_div_frame).on('mousemove', function(e){
        x = e.pageX;
        y = e.pageY;
        //var length = current_text_boxes_coords.length;
        //var i = 0;
        current_text_boxes_coords.forEach(function(row){
            if(row.x1 < x && row.x2 > x && row.y1 < y && row.y2 > y){
                $(row.box).parent().css('z-index', text_layer_z_up);
            }
        });
    });

    $('#submit_lightbox_button').mousedown(function(){$('#submit_lightbox_button').addClass('mousedown')});
    $('#submit_lightbox_button').mouseup(function(){$('#submit_lightbox_button').removeClass('mousedown')});
    $('#submit_lightbox_button').click(function(){$('#submit_lightbox_outer').show();});

    $('#close_submit_lightbox').click(function(){$('#submit_lightbox_outer').hide();});

    var pages_button = document.getElementById('pages_button');
    var pages_button_interval = setInterval(function() {
        pages_button.style.color = (pages_button.style.color == 'red' ? '' : 'red');
    }, 1000);
    setTimeout(function(){clearInterval(pages_button_interval);}, 20000);
    $(pages_button).mouseover(function(){clearInterval(pages_button_interval);$(pages_button).off('mouseover');$(pages_button).css('color', 'black');});
    $(pages_button).click(secondaryMenu);
    $('.page_list_item').click(secondaryMenu);

    secondaryMenu();

    $(preload_screen_id).hide();
});