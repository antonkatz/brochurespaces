import BrochureSpaces
import logging
import pickle

logging.basicConfig(level=logging.DEBUG)

pg = BrochureSpaces.pg.Storage()
box = BrochureSpaces.box.Box(storage=pg)
links = box.shareFolderFiles(folder_id=1134611025)
#links = box.shareFile(file_id=11367543966, skip_refresh=True)

print links

pickle_file = open('test_links.p', 'wb')

_links = []
for i in links:
    _links.append({'name': 'Test_Image', 'server_link': i})

print _links

pickle.dump(_links, pickle_file)